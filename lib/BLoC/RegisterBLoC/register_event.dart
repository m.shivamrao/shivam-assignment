part of 'register_bloc.dart';

@immutable
abstract class RegisterEvent {}

class OnChangeTextFieldValueEvent extends RegisterEvent {
  final String phoneNumberValue;
    final String name;
  OnChangeTextFieldValueEvent(this.phoneNumberValue, this.name);
}


class OnRegisterButtonPressedEvent extends RegisterEvent{
  final String phoneNumber;
  final String name;

  OnRegisterButtonPressedEvent(this.phoneNumber, this.name);
}
