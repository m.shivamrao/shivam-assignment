import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc() : super(RegisterInitial()) {
    on<OnChangeTextFieldValueEvent>((event, emit) => {
          if (event.phoneNumberValue.length != 10 || event.name.isEmpty)
            {emit(InvalidRegistrationFormState("Invalid Phone number or Name"))}
          else
            {emit(ValidRegistrationFormState())}
        });
    on<OnRegisterButtonPressedEvent>((event, emit) {});
  }
}
