part of 'register_bloc.dart';

@immutable
abstract class RegisterState {}

class RegisterInitial extends RegisterState {}

class InvalidRegistrationFormState extends RegisterState{
  String errMsg;

  InvalidRegistrationFormState(this.errMsg);
}

class ValidRegistrationFormState extends RegisterState{}
