part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class InvalidLoginFormState extends LoginState {
  String errMsg;
  InvalidLoginFormState(this.errMsg);
}

class ValidLoginFormState extends LoginState {}

class GetOtpLoadingState extends LoginState {}
