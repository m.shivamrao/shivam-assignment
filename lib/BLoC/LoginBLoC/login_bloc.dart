import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial()) {
    on<OnChangeLoginTextFieldValueEvent>((event, emit) {
      if (event.phoneNumberValue.length != 10) {
        emit(InvalidLoginFormState("Please enter a valid phone number"));
      } else {
        emit(ValidLoginFormState());
      }
    });
    on<OnLoginButtonPressedEvent>((event, emit) {
      if (event.phoneNumber.length != 10) {
        emit(InvalidLoginFormState("Please enter a valid phone number"));
      } else {
        emit(GetOtpLoadingState());
      }
    });
  }
}
