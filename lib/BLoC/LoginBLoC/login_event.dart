part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class OnLoginButtonPressedEvent extends LoginEvent {
  String phoneNumber;
  OnLoginButtonPressedEvent(this.phoneNumber);
}

class OnChangeLoginTextFieldValueEvent extends LoginEvent {
  String phoneNumberValue;
  OnChangeLoginTextFieldValueEvent(this.phoneNumberValue);
}
