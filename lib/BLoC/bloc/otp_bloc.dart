import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'otp_event.dart';
part 'otp_state.dart';

class OtpBloc extends Bloc<OtpEvent, OtpState> {
  OtpBloc() : super(OtpInitial()) {
    on<OnOtpSubmitEvent>((event, emit) {
      if (event.otpValue.length != 6) {
        emit(OtpFieldInvalidState("Invalid OTP "));
      } else {
        emit(OtpFieldValidState());
      }
    });
  }
}
