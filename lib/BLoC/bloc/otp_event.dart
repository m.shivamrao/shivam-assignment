part of 'otp_bloc.dart';

@immutable
abstract class OtpEvent {}

class OtpInitialEvent extends OtpEvent {}

class OnOtpSubmitEvent extends OtpEvent {
  String otpValue;
  OnOtpSubmitEvent(this.otpValue);
}
