part of 'otp_bloc.dart';

@immutable
abstract class OtpState {}

class OtpInitial extends OtpState {}

class OtpFieldInvalidState extends OtpState {
  String errMsg;
  OtpFieldInvalidState(this.errMsg);
}

class OtpFieldValidState extends OtpState {}

class OnSubmitOtpState extends OtpState {}
