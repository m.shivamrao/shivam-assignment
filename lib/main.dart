import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:vivatech_assignment/BLoC/LoginBLoC/login_bloc.dart';
import 'package:vivatech_assignment/firebaseServices/firebase_service.dart';
import 'package:vivatech_assignment/firebase_options.dart';
import 'package:vivatech_assignment/screens/login_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vivatech_assignment/screens/OTPScreent.dart';
import 'package:vivatech_assignment/screens/RegistrationScreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  await FirebaseService.initFirebaseMessaging();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        "/register": (context) => RegisterScreen(),
        "/login": (context) => LoginScreen(),
      },
      home: BlocProvider(
        create: (context) => LoginBloc(),
        child: LoginScreen(),
      ),
    );
  }
}
