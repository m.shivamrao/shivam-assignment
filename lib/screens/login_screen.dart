import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vivatech_assignment/BLoC/LoginBLoC/login_bloc.dart';
import 'package:vivatech_assignment/BLoC/RegisterBLoC/register_bloc.dart';
import 'package:vivatech_assignment/firebaseServices/firebase_service.dart';
import 'package:vivatech_assignment/screens/RegistrationScreen.dart';
import 'package:vivatech_assignment/widgets/AppHeader.dart';
import 'package:vivatech_assignment/widgets/CustomButton.dart';
import 'package:vivatech_assignment/widgets/custom_text_field.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController phoneController = TextEditingController();

  TextEditingController otpController = TextEditingController();

  String? verificationId;

  final loginFormKey = GlobalKey<FormState>();

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            toolbarHeight: 150,
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            titleTextStyle: const TextStyle(
              color: Colors.black,
            ),
            title: const AppHeader(
                helperText: "Please enter your Phone Number \n and OTP"),
          ),
          body: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 50.0, vertical: 40.0),
            child: Form(
              key: loginFormKey,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomTextField(
                      maxLength: 10,
                      onChanged: (val) {
                        BlocProvider.of<LoginBloc>(context).add(
                            OnChangeLoginTextFieldValueEvent(
                                phoneController.text.trim()));
                      },
                      iconData: Icons.phone_android_rounded,
                      keyboardType: TextInputType.phone,
                      hintText: "Enter your Phone Number",
                      isPswdField: false,
                      textEditingController: phoneController,
                    ),
                    const SizedBox(height: 20),
                    BlocBuilder<LoginBloc, LoginState>(
                        builder: (context, state) {
                      if (state is InvalidLoginFormState) {
                        return Text(
                          state.errMsg,
                          style: TextStyle(color: Colors.red),
                        );
                      } else {
                        return Container();
                      }
                    }),
                    SizedBox(height: 40),
                    isLoading
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : BlocBuilder<LoginBloc, LoginState>(
                            builder: (context, state) {
                              return GestureDetector(
                                onTap: (state is ValidLoginFormState)
                                    ? () async {
                                        BlocProvider.of<LoginBloc>(context)
                                            .emit(GetOtpLoadingState());
                                        await FirebaseService().getOtp(
                                            phoneController.text.trim(),
                                            context);
                                      }
                                    : null,
                                child: (state is GetOtpLoadingState)
                                    ? CircularProgressIndicator()
                                    : CustomButton(
                                        btnText: "Get OTP",
                                        fontSize: 20.0,
                                        startEndPadding: 60,
                                        topBottomPadding: 15,
                                        isDisable:
                                            (state is InvalidLoginFormState)
                                                ? true
                                                : false,
                                      ),
                              );
                            },
                          ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          "Not registered?",
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (builder) {
                                  return BlocProvider(
                                    create: (context) => RegisterBloc(),
                                    child: RegisterScreen(),
                                  );
                                },
                              ),
                            );
                          },
                          child: const Text(
                            "Sign Up",
                            style: TextStyle(
                              fontSize: 15,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
