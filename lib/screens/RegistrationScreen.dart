import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/otp_field_style.dart';
import 'package:vivatech_assignment/BLoC/LoginBLoC/login_bloc.dart';
import 'package:vivatech_assignment/BLoC/RegisterBLoC/register_bloc.dart';
import 'package:vivatech_assignment/firebaseServices/firebase_service.dart';
import 'package:vivatech_assignment/widgets/AppHeader.dart';
import 'package:vivatech_assignment/widgets/CustomButton.dart';
import 'package:vivatech_assignment/widgets/custom_text_field.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController phoneController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  TextEditingController nameController = TextEditingController();

  String errText = "";

  final registerFormKey = GlobalKey<FormState>();

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            toolbarHeight: 150,
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            titleTextStyle: const TextStyle(
              color: Colors.black,
            ),
            title: const AppHeader(
                helperText:
                    "Please enter your e-mail address \n and enter password"),
          ),
          body: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 30.0, vertical: 40.0),
            child: SingleChildScrollView(
              child: Form(
                key: registerFormKey,
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      CustomTextField(
                        maxLength: 20,
                        iconData: Icons.person_outline_rounded,
                        hintText: "Enter your name",
                        isPswdField: false,
                        keyboardType: TextInputType.text,
                        textEditingController: nameController,
                        onChanged: (val) {
                          BlocProvider.of<RegisterBloc>(context).add(
                              OnChangeTextFieldValueEvent(
                                  phoneController.text.trim(),
                                  nameController.text.trim()));
                        },
                        // errText: errText,
                      ),
                      CustomTextField(
                        maxLength: 10,
                        iconData: Icons.phone_android_rounded,
                        hintText: "Enter your Phone Number",
                        isPswdField: false,
                        keyboardType: TextInputType.phone,
                        textEditingController: phoneController,
                        onChanged: (val) {
                          BlocProvider.of<RegisterBloc>(context)
                              .add(OnChangeTextFieldValueEvent(
                            phoneController.text,
                            nameController.text,
                          ));
                        },
                        // errText: errText,
                      ),
                      BlocBuilder<RegisterBloc, RegisterState>(
                          builder: (context, state) {
                        if (state is InvalidRegistrationFormState) {
                          return Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              state.errMsg,
                              style: TextStyle(color: Colors.red),
                            ),
                          );
                        } else {
                          return Container();
                        }
                      }),
                      const SizedBox(
                        height: 80,
                      ),
                      isLoading
                          ? const Center(
                              child: CircularProgressIndicator(),
                            )
                          : BlocBuilder<RegisterBloc, RegisterState>(
                              builder: (context, state) {
                                return GestureDetector(
                                  onTap: (state is ValidRegistrationFormState)
                                      ? () async {
                                          isLoading = true;
                                          // setState(() {});
                                          await FirebaseService().getOtp(
                                              phoneController.text.trim(),
                                              context);
                                        }
                                      : null,
                                  child:  CustomButton(
                                    btnText: "Get OTP",
                                    fontSize: 20.0,
                                    startEndPadding: 60,
                                    topBottomPadding: 15,
                                    isDisable:
                                        (state is InvalidRegistrationFormState)
                                            ? true
                                            : false,
                                  ),
                                );
                              },
                            ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            "Already registered?",
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 15,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: const Text(
                              "Sign In",
                              style: TextStyle(
                                fontSize: 15,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
