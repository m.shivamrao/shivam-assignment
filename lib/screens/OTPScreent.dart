import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:vivatech_assignment/BLoC/bloc/otp_bloc.dart';
import 'package:vivatech_assignment/firebaseServices/firebase_service.dart';
import 'package:vivatech_assignment/widgets/AppHeader.dart';
import 'package:vivatech_assignment/widgets/CustomButton.dart';

class OtpScreen extends StatelessWidget {
  OtpScreen({super.key, required this.verificationId});

  final String verificationId;
  String? smsCode;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            leadingWidth: 1,
            toolbarHeight: 150,
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            titleTextStyle: const TextStyle(
              color: Colors.black,
            ),
            title: const AppHeader(
                helperText: "Please enter your Phone Number \n and OTP"),
          ),
          body: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 50.0, vertical: 40.0),
            child: Form(
              // key: loginFormKey,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    OTPTextField(
                        // controller: otpController,
                        length: 6,
                        width: MediaQuery.of(context).size.width,
                        textFieldAlignment: MainAxisAlignment.spaceAround,
                        fieldWidth: 40,
                        fieldStyle: FieldStyle.box,
                        outlineBorderRadius: 10,
                        style: const TextStyle(fontSize: 20),
                        onChanged: (pin) {},
                        onCompleted: (pin) {
                          smsCode = pin;
                        }),
                    BlocBuilder<OtpBloc, OtpState>(builder: (context, state) {
                      if (state is OtpFieldInvalidState) {
                        return Text(
                          state.errMsg,
                          style: TextStyle(color: Colors.red),
                        );
                      } else {
                        return Container();
                      }
                    }),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          "Didn't receive code?",
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        TextButton(
                          onPressed: () {
                            Fluttertoast.showToast(msg: "Not Implemented Yet");
                          },
                          child: const Text(
                            "Request again",
                            style: TextStyle(
                              fontSize: 15,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 50),
                    GestureDetector(
                      onTap: () async {
                        if (smsCode != null) {
                          await FirebaseService()
                              .validateOtp(verificationId, smsCode!);
                        } else {
                          Fluttertoast.showToast(
                              msg: "Error! Please enter OTP");
                        }
                      },
                      child: const CustomButton(
                          btnText: "LOGIN",
                          fontSize: 20,
                          startEndPadding: 50,
                          topBottomPadding: 10,
                          isDisable: false),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
