import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField(
      {super.key,
      required this.hintText,
      required this.iconData,
      required this.isPswdField,
      this.showHidePassword,
      required this.textEditingController,
      this.errText,
      this.onChanged,
      required this.keyboardType,
      this.validate,
      required this.maxLength});

  final bool isPswdField;
  final TextInputType keyboardType;
  final String? hintText;
  final IconData? iconData;
  final Function()? showHidePassword;
  final TextEditingController textEditingController;
  final String? errText;
  final Function(String)? onChanged;
  final String? Function(String?)? validate;
  final int? maxLength;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChanged,
      validator: validate,
      keyboardType: keyboardType,
      maxLength: maxLength,
      decoration: InputDecoration(
        counterText: "",
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(30),
        ),
        errorText: errText,
        fillColor: Colors.grey.withOpacity(0.1),
        filled: true,
        prefixIcon: Icon(
          iconData,
          color: Colors.grey.withOpacity(0.5),
        ),
        suffixIcon: isPswdField
            ? IconButton(
                onPressed: () {}, icon: const Icon(Icons.visibility_off))
            : null,
        hintText: hintText,
        hintStyle: TextStyle(
          color: Colors.grey.withOpacity(0.5),
          fontWeight: FontWeight.w500,
          fontSize: 20,
        ),
      ),
      controller: textEditingController,
    );
  }
}
