import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton(
      {super.key,
      required this.btnText,
      required this.fontSize,
      required this.startEndPadding,
      required this.topBottomPadding,
      required this.isDisable});

  final String btnText;
  final double fontSize;
  final double startEndPadding;
  final double topBottomPadding;
  final bool isDisable;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: isDisable ? Colors.grey: Colors.black,
        borderRadius: BorderRadius.circular(30),
      ),

      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: startEndPadding, vertical: topBottomPadding),
        child: Text(
          btnText,
          style: TextStyle(fontSize: fontSize, color: Colors.white),
        ),
      ),
    );
  }
}
