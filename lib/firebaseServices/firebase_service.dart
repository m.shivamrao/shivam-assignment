import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:vivatech_assignment/BLoC/bloc/otp_bloc.dart';
import 'package:vivatech_assignment/firebaseServices/local_notification_service.dart';
import 'package:vivatech_assignment/screens/OTPScreent.dart';

class FirebaseService {
  static final FirebaseMessaging _firebaseMessaging =
      FirebaseMessaging.instance;
  static final _localNotification = FlutterLocalNotificationsPlugin();

  Future<void> getOtp(String phone, BuildContext context) async {
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: "+91 $phone",
      verificationCompleted: (credentials) {},
      verificationFailed: (ex) {},
      codeSent: (verificationId, resendToken) {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (builder) => BlocProvider(
              create: (context) => OtpBloc(),
              child: OtpScreen(
                verificationId: verificationId,
              ),
            ),
          ),
        );
      },
      codeAutoRetrievalTimeout: (verificationId) {
        // Fluttertoast.showToast(msg: "Otp Timed Out");
      },
    );
  }

  Future validateOtp(String verificationId, String smsCode) async {
    PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId, smsCode: smsCode);

    try {
      var userCredential =
          await FirebaseAuth.instance.signInWithCredential(credential);
      if (userCredential.user != null) {
        Fluttertoast.showToast(msg: "Success");
      }
    } on FirebaseAuthException catch (ex) {
      Fluttertoast.showToast(msg: ex.message.toString());
    }
  }

  static Future initFirebaseMessaging() async {
    await _firebaseMessaging.requestPermission();
    var fcmToken = await _firebaseMessaging.getToken();
    print("=======================> \n $fcmToken");
    initPushNotification();
    initLocalNotification();
  }

  static Future initPushNotification() async {
    FirebaseMessaging.instance.getInitialMessage().then((value) {});
    FirebaseMessaging.onMessageOpenedApp.listen((message) async {});
    FirebaseMessaging.onBackgroundMessage((message) async {
      print(message.notification?.title);
      print(message.notification?.body);
      print(message.data);
    });

    FirebaseMessaging.onMessage.listen((msg) {
      final notification = msg.notification;
      if (notification == null) return;
      _localNotification.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            FlutterLocalNotification.androidChannel.id,
            FlutterLocalNotification.androidChannel.name,
            icon: "@drawable/ic_launcher",
            channelDescription:
                FlutterLocalNotification.androidChannel.description,
          ),
        ),
        payload: jsonEncode(msg.toMap()),
      );
    });
  }

  static Future initLocalNotification() async {
    const androidSetting = AndroidInitializationSettings("@drawable/ic_launcher");
    const setting = InitializationSettings(android: androidSetting);

    await _localNotification.initialize(
      setting,
    );

    final platform = _localNotification.resolvePlatformSpecificImplementation<
        AndroidFlutterLocalNotificationsPlugin>();

    await platform?.createNotificationChannel(FlutterLocalNotification.androidChannel);
  }
}
