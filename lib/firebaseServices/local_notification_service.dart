import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class FlutterLocalNotification {
  static final androidChannel = AndroidNotificationChannel(
      "vivatech_assignment_channel", "VIVATECH ASSIGNMENT CHANNEL",
  importance: Importance.defaultImportance);
}
